$(function () {

  $(".main-news .slider").slick({
    slide: 'article',
    slidesToShow: 1,
    slidesToScroll: 1,
    slidesPerRow: 1,
    speed: 1200,
    rtl: true,
    dots: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: false
  });


  var isNotificaionGot = false;
  function sendNewsLetter(email) {
   
  }

  // Get Notofication
  function getNotifications() {
    
    
}



  $(".homeNotification-btn").on("click", function () {
    $(this).toggleClass("is-open");
    $(".homeNotification-panel").toggleClass("is-open");
  });

});

// progressbar.js@1.0.0 version is used
// Docs: http://progressbarjs.readthedocs.org/en/1.0.0/

var bar = new ProgressBar.Line('#vacancyProgress', {
  strokeWidth: 4,
  easing: 'easeInOut',
  duration: 2500,
  color: '#066460',
  trailColor: '#e0e2ea',
  trailWidth: 4,
  round: true,
  svgStyle: { width: '100%', height: '100%' },
  text: {
    style: {
      // Text color.
      // Default: same as stroke color (options.color)
      color: '#fff',
      position: 'absolute',
      right: '0',
      top: '-25px',
      padding: '3px 8px',
      margin: 0,
      'font-size': '0.6rem',
      background: '#066460',
      'border-radius': '5px',

      transform: null
    },
    autoStyleContainer: false
  },
  from: { color: '#FFEA82' },
  to: { color: '#ED6A5A' },
  step: (state, bar) => {
    bar.setText(Math.round(bar.value() * 100) + ' %');
  }
});

bar.animate(0.6);  // Number from 0.0 to 1.0

var bar2 = new ProgressBar.Line('#askProgress', {
  strokeWidth: 4,
  easing: 'easeInOut',
  duration: 2500,
  color: '#066460',
  trailColor: '#e0e2ea',
  trailWidth: 4,
  round: true,
  svgStyle: { width: '100%', height: '100%' },
  text: {
    style: {
      // Text color.
      // Default: same as stroke color (options.color)
      color: '#fff',
      position: 'absolute',
      right: '0',
      top: '-25px',
      padding: '3px 8px',
      margin: 0,
      'font-size': '0.6rem',
      background: '#066460',
      'border-radius': '5px',

      transform: null
    },
    autoStyleContainer: false
  },
  from: { color: '#FFEA82' },
  to: { color: '#ED6A5A' },
  step: (state, bar) => {
    bar.setText(Math.round(bar.value() * 100) + ' %');
  }
});

bar2.animate(0.8);  // Number from 0.0 to 1.0
